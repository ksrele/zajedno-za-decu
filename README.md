# zajedno-za-decu

# Requirements

* Docker
* Docker compose

# Installation

```
docker-compose up
```

Kad zavrsi, u drugom terminalu poterati ovo (samo prvi put treba):

```
docker-compose exec laravel-app composer install
docker-compose exec laravel-app php artisan key:generate
```



Dockerfile i docker-compose pokupljen odavde:

<https://dev.to/veevidify/docker-compose-up-your-entire-laravel-apache-mysql-development-environment-45ea>

